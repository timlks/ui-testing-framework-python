# UI Testing Framework Python

# Prerequisites:
# 1. Download chromedriver and add it to the Path
# 2. Download and install JetBrains PyCharm, which is the preferred IDE
# 3. Have python 3.7 installed (Note: Not sure if it will run with earlier version)
# 4. Configure the Python Interpreter to install behave 1.2.6, pip 19.0.3, parameterized 0.7.0, PyExecJS 1.5.1, and selenium 3.141.0
#
# How to run:
# In the command line enter behave and the path to the feature file eg. ../tests/resources/SignUp.feature
#
# Argument Options:
# You can also add additional arguments, following each the leter -D. 
# The following options are available:
# 1. For using a browser, add chrome / ie / firefox (Note: Only chrome works for now)
# 2. For using the gird, add true / false
# 3. For using or not a headless browser, add isheadless / nonheadless
#
# The arguments can be in any order, and in either lower or upper case.
# If you enter a n argument wrong, a default value will be used
# Example of passing multiple arguments: 
# behave (path to the feature file) -D chrome -D false -D nonheadless
# 
# Note 1: Grid option is not working for now
# Note 2: Only chrome works for now
# Note 3: You can test one feature at a time for now