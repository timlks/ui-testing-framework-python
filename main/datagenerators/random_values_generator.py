import random, string
from random import randint

class RandomDataGenerator:

    @staticmethod
    def randonString():
        return ''.join(random.choice(string.ascii_letters.lower() + string.digits) for _ in range(10))

    @staticmethod
    def randonInt(length):
        return ''.join(["%s" % randint(0, 9) for num in range(0, length)])

    @staticmethod
    def randomEmail():
        return RandomDataGenerator.randonString() + "@test.com"