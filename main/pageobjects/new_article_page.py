from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

class NewArticlePage():

    driver = None

    locator_dictionary = {
        "title": (By.XPATH, "//input[@formcontrolname='title']"),
        "description": (By.XPATH, "//input[@formcontrolname='description']"),
        "body": (By.XPATH, "//textarea[@formcontrolname='body']"),
        "tags": (By.XPATH, "//fieldset[4]/input"),
        "publishButton": (By.XPATH, "//form/fieldset/button")
    }

    def __init__(self, driver):
        self.driver = driver

    def clickPublishButton(self):
        try:
            self.driver.find_element(*self.locator_dictionary.get('publishButton')).click()
        except:
            raise NoSuchElementException("The publish button was not found")

    def setTitle(self, titleToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('title'))
            element.clear()
            element.send_keys(titleToEnter)
        except:
            raise NoSuchElementException("The title field was not found")

    def setDescription(self, descriptionToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('description'))
            element.clear()
            element.send_keys(descriptionToEnter)
        except:
            raise NoSuchElementException("The description field was not found")

    def setBody(self, bodyToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('body'))
            element.clear()
            element.send_keys(bodyToEnter)
        except:
            raise NoSuchElementException("The body field was not found")

    def setTags(self, tagsToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('tags'))
            element.clear()

            for tag in tagsToEnter:
                element.send_keys(tag)
                element.send_keys(Keys.ENTER)
        except:
            raise NoSuchElementException("The tags field was not found")
