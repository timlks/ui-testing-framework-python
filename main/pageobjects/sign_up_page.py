from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class SignUpPage():

    driver = None

    locator_dictionary = {
        "username": (By.XPATH, "//input[@formcontrolname='username']"),
        "email": (By.XPATH, "//input[@formcontrolname='email']"),
        "password": (By.XPATH, "//input[@formcontrolname='password']"),
        "signUpButton": (By.XPATH, "//form/fieldset/button")
    }

    def __init__(self, driver):
        self.driver = driver

    def setUsername(self, usernameToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('username'))
            element.clear()
            element.send_keys(usernameToEnter)
        except:
            raise NoSuchElementException("The username field was not found")

    def setEmail(self, emailToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('email'))
            element.clear()
            element.send_keys(emailToEnter)
        except:
            raise NoSuchElementException("The email field was not found")

    def setPassword(self, passwordToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('password'))
            element.clear()
            element.send_keys(passwordToEnter)
        except:
            raise NoSuchElementException("The password field was not found")

    def clickSignUp(self):
        try:
            self.driver.find_element(*self.locator_dictionary.get('signUpButton')).click()
        except:
            raise NoSuchElementException("The sign up button was not found")

    def createUser(self, userProfile):
        self.setUsername(userProfile.username)
        self.setPassword(userProfile.password)
        self.setEmail(userProfile.email)
        self.clickSignUp()