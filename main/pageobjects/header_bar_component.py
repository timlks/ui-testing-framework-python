from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class HeaderBarComponent:

   driver = None

   locator_dictionary = {
       "profileName": (By.XPATH, "//nav/div/ul/li[4]/a"),
       "newArticle": (By.XPATH, "//nav/div/ul/li[2]/a"),
       "signIn": (By.XPATH, "//nav/div/ul/li[2]/a"),
       "signUp": (By.XPATH, "//nav/div/ul/li[3]/a"),
       "home": (By.XPATH, "//nav/div/ul/li[1]/a"),
       "logo": (By.XPATH, "//app-layout-header/nav/div/a"),
       "settings": (By.XPATH, "//nav/div/ul/li[3]/a")
   }

   def __init__(self, driver):
       self.driver = driver

   def clickSettings(self):
        try:
            self.driver.find_element(*self.locator_dictionary.get('settings')).click()
        except:
            raise NoSuchElementException("The settings button was not found")

   def clickSignInButton(self):
        try:
            self.driver.find_element(*self.locator_dictionary.get('signIn')).click()
        except:
            raise NoSuchElementException("The sign in button was not found")

   def clickSignUp(self):
        try:
            self.driver.find_element(*self.locator_dictionary.get('signUp')).click()
        except:
            raise NoSuchElementException("The sign up button was not found")

   def clickNewArticle(self):
       try:
           self.driver.find_element(*self.locator_dictionary.get('newArticle')).click()
       except:
           raise NoSuchElementException("The article button was not found")

   def isProfileNameDisplayed(self):
       return self.driver.find_element(*self.locator_dictionary.get('profileName')).is_displayed()

   def isSignInDisplayed(self):
       return self.driver.find_element(*self.locator_dictionary.get('signIn')).is_displayed()

   def getProfileName(self):
       return self.driver.find_element(*self.locator_dictionary.get('profileName')).text

   def isSettingsButtonActive(self):
       return self.driver.find_element(*self.locator_dictionary.get('settings')).is_enabled()
