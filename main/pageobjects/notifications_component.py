from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class NotificicationsComponent():

    driver = None

    locator_dictionary = {
        "notifications": (By.CLASS_NAME, "error-messages"),
    }

    def __init__(self, driver):
        self.driver = driver

    def areNotificationsDisplayed(self):
        if len(self.getNotifications()) > 0:
            return True

        return False;

    def isNotificationDisplayed(self, notification):
        for notificationDisplayed in self.getNotifications():
            if notificationDisplayed.text == notification:
                return True

    def getNotifications(self):
        notifications = self.driver.find_element(*self.locator_dictionary.get('notifications'))
        return notifications.find_elements(By.TAG_NAME, "li")