from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class SettingsPage():

    driver = None

    locator_dictionary = {
        "signUpLabel": (By.CLASS_NAME, "text-xs-center"),
        "logoutButton": (By.XPATH, "//div/div/div/div/button")
    }

    def __init__(self, driver):
        self.driver = driver

    def clickLogout(self):
        try:
            self.driver.find_element(*self.locator_dictionary.get('logoutButton')).click()
        except:
            raise NoSuchElementException("The logout button was not found")
