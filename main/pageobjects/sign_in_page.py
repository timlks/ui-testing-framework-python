from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class SignInPage():

    driver = None

    locator_dictionary = {
        "email": (By.XPATH, "//input[@formcontrolname='email']"),
        "password": (By.XPATH, "//input[@formcontrolname='password']"),
        "signIn": (By.XPATH, "//form/fieldset/button")
    }

    def __init__(self, driver):
        self.driver = driver

    def setEmail(self, emailToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('email'))
            element.clear()
            element.send_keys(emailToEnter)
        except:
            raise NoSuchElementException("The email field was not found")

    def setPassword(self, passwordToEnter):
        try:
            element = self.driver.find_element(*self.locator_dictionary.get('password'))
            element.clear()
            element.send_keys(passwordToEnter)
        except:
            raise NoSuchElementException("The password field was not found")

    def clickSignIn(self):
        try:
            self.driver.find_element(*self.locator_dictionary.get('signIn')).click()
        except:
            raise NoSuchElementException("The sign in button was not found")

    def addCredentials(self, userProfile):
        self.setEmail(userProfile.email)
        self.setPassword(userProfile.password)
        self.clickSignIn()