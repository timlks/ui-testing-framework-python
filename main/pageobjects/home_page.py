from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class HomePage():

    driver = None

    locator_dictionary = {
        "yourFeed": (By.XPATH, "//div[1]/div/ul/li[1]/a"),
        "globalFeed": (By.XPATH, "//div[1]/div/ul/li[2]/a"),
        "articleList": (By.XPATH, "//app-article-list")
    }

    def __init__(self, driver):
        self.driver = driver

    def getArticleList(self):
        articleList = self.driver.find_element(*self.locator_dictionary.get('articleList'))
        list = articleList.find_elements_by_xpath('.//app-article-preview')

        return list

    def containsArticle(self, title):
        for element in self.getArticleList():
            if element.find_element_by_xpath('.//h1').text == title:
                return True
        return False

    def selectFeed(self, feed):
        try:
            if feed == "your":
                self.driver.find_element(*self.locator_dictionary.get('yourFeed')).click()
            else:
                self.driver.find_element(*self.locator_dictionary.get('globalFeed')).click()
        except:
            raise NoSuchElementException("The feed field was not found")

