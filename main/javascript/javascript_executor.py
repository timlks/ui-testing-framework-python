import execjs
import os
from os import path
import time

class JavascriptExecutor:

    script = None
    defaultConfigPath = os.path.dirname(__file__) + "/../../test/resources/scripts/waitForAngular.js"

    def __init__(self):
        self.loadJavascriptFile()

    def loadJavascriptFile(self):
        if path.exists(self.defaultConfigPath):
            with open(self.defaultConfigPath, "r") as file:
                self.script = file.read()
        else:
            raise FileNotFoundError(f"File could not be found in: {self.defaultConfigPath}")

    def executeJavascript(self):
        seg = self.script
        ctx = execjs.compile(self.script)

        try:
            ctx.call("waitForAngular")
        except Exception as error:
            print(error)
            time.sleep(1)