from .session import Session

class SessionSingleton:

    sessionInstance = None

    @staticmethod
    def getSession():
        if SessionSingleton.sessionInstance == None:
            sessionInstance = Session()
        return sessionInstance


