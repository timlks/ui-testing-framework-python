from test.configuration.environment_configuration import EnvironmentConfig
from test.configuration.browser_profile_configuration import BrowserProfileConfig
from test.configuration.driver_configuration import DriverConfiguration
from test.configuration.grid_configuration import GridConfig
from test.driverfactory.driver_manager_factory import DriverManagerFactory
from main.javascript.javascript_executor import JavascriptExecutor
import sys

class Session:
    driverManager = None
    useGrid = None
    configuration = None
    browserProfileConfig = None
    sharedData = None
    driverType = None
    javascriptExecutor = None

    def __init__(self):
        self.configuration = EnvironmentConfig()
        self.browserProfileConfig = BrowserProfileConfig(self.configureProfile())
        self.useGrid = self.configureGrid()
        self.driverType = self.configureDriverType()
        self.driverManager = self.createManager()
        self.sharedData = dict()
        self.javascriptExecutor = JavascriptExecutor()

    def createManager(self):
        if self.useGrid.lower() == "false":
            driverConfiguration = DriverConfiguration(self.driverType, self.browserProfileConfig.isHeadless,
                                                      self.browserProfileConfig.browserVersion)
            self.driverManager = DriverManagerFactory.getLocalManager(driverConfiguration)
            return self.driverManager
        else:
            gridConfig = GridConfig()
            driverConfiguration = DriverConfiguration(self.driverType, self.configureProfile(),
                                                      self.browserProfileConfig.browserVersion, gridConfig.platform,
                                                      gridConfig.getHubUrl())
            self.driverManager = DriverManagerFactory.getRemoteManager(driverConfiguration)

    def configureDriverType(self):
        arguments = sys.argv
        for argument in arguments:
            if argument.lower() == "chrome" or argument.lower() == "firefox" or argument.lower() == "ie":
                self.driverType = argument

        if self.driverType == None:
            return "chrome"
        else:
            return self.driverType

    def configureGrid(self):
        arguments = sys.argv
        for argument in arguments:
            if argument.lower() == "true" or argument.lower() == "false":
                self.useGrid = argument

        if self.useGrid == None:
            return "false"
        else:
            return self.useGrid

    def configureProfile(self):
        profile = None
        arguments = sys.argv
        for argument in arguments:
            if argument.lower() == "isheadless" or argument.lower() == "nonheadless":
                profile = argument

        if profile==None:
            return "nonheadless"
        else:
            return profile

    def getBaseUrl(self):
        return self.configuration.baseUrl

    def getDriver(self):
        if self.driverManager.driver == None:
            self.driverManager.driver = self.driverManager.createDriver()
        return self.driverManager.driver

    def stopDriver(self):
        return self.driverManager.stopDriver()

    def waitForAngular(self):
        self.javascriptExecutor.executeJavascript()
