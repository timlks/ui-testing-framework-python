from .drivermanagers.chrome_driver_manager import ChromeDriverManager
from .drivermanagers.firefox_driver_manager import FirefoxDriverManager
from .drivermanagers.ie_driver_manager import IEDriverManager

class DriverManagerFactory:

    @staticmethod
    def getLocalManager(configuration):

        if configuration.driverType.lower() == "chrome":
            driverManager = ChromeDriverManager(configuration)
        elif configuration.driverType.lower() == "firefox":
            driverManager = FirefoxDriverManager(configuration)
        else:
            driverManager = IEDriverManager(configuration)

        return driverManager

    def getRemoteManager(self, configuration):
        pass