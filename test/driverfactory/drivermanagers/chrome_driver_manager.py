from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class ChromeDriverManager():

    configuration = None
    driver = None

    def __init__(self, configuration):
        self.configuration = configuration

    def createDriver(self):
        if self.driver == None:
            if self.configuration.isHeadless.lower() == "false":
                chrome_options = Options()
                chrome_options.add_argument("disable-infobars")
                return webdriver.Chrome(chrome_options=chrome_options)
            else:
                chrome_options = Options()
                chrome_options.add_argument("--headless")
                return webdriver.Chrome(chrome_options=chrome_options)
        else:
            return self.driver

    def stopDriver(self):
        if self.driver != None:
            self.driver.close()
            self.driver = None