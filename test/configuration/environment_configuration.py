from .configuration_manager import ConfigurationManager
import json


class EnvironmentConfig(ConfigurationManager):

    config = None
    baseUrl = None
    configFile = None

    def __init__(self):
        self.configFile = self.getConfigFile()
        self.loadConfigFile()
        self.loadConfiguration()

    def loadConfigFile(self):
        with open(self.configFile) as file:
            self.config = json.load(file)

    def loadConfiguration(self):
        self.baseUrl = self.config['vdiLocal']['base-url']