from .configuration_manager import ConfigurationManager
import json

class BrowserProfileConfig(ConfigurationManager):

    config = None
    isHeadless = None
    browserVersion = None
    configFile = None

    def __init__(self, profile):
        self.configFile = self.getConfigFile()
        self.loadConfigFile(profile)
        self.loadConfiguration()

    def loadConfigFile(self, profile):
        with open(self.configFile) as file:
            self.config = json.load(file)

            if profile == 'isheadless':
                self.config = self.config["headless"][0]
            elif profile == 'nonheadless':
                self.config = self.config["headless"][1]

    def loadConfiguration(self):
        self.isHeadless = self.config['run-headless']
        self.browserVersion = self.config['browser-version']


