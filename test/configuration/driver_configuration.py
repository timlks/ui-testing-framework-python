
class DriverConfiguration:

    driverType = None
    isHeadless = None
    browserVersion = None
    platform = None
    hubUrl = None

    def __init__(self, driverType, isHeadless, browserVersion, platform = None, hubUrl = None):
        self.driverType = driverType
        self.isHeadless = isHeadless
        self.browserVersion = browserVersion
        self.platform = platform
        self.hubUrl = hubUrl

