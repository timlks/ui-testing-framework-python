from .configuration_manager import ConfigurationManager
import json

class GridConfig(ConfigurationManager):

    config = None
    platform = None
    hubDomain = None
    hubPort = None
    configFile = None

    def __init__(self):
        self.configFile = self.getConfigFile()
        self.loadConfigFile()
        self.loadConfiguration()

    def loadConfigFile(self, profile):
        with open(self.configFile) as file:
            self.config = json.load(file)
            self.config = self.config["gridConfig"]


    def loadConfiguration(self):
        self.platform = self.config['platform']
        self.hubDomain = self.config['hub-domain']
        self.hubPort = self.config['hub-port']

    def getHubUrl(self):
        return f"{self.hubDomain}:{self.hubPort}/wd/hub"