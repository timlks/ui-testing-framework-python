from os import environ
from os import path
import os

class ConfigurationManager:

    defaultConfigPath = os.path.dirname(__file__) + "/../resources/config/environments.json"

    def getConfigFile(self):

        configPath = environ.get("testConfig")

        if configPath is not None:
            if path.exists(configPath):
                return configPath
            else:
                raise FileNotFoundError(f"File could not be found in: {configPath}")
        else:
            if path.exists(self.defaultConfigPath):
                return self.defaultConfigPath
            else:
                raise FileNotFoundError(f"File could not be found in: {self.defaultConfigPath}")

