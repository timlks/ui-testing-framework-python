from main.pageobjects.sign_up_page import SignUpPage
from main.pageobjects.sign_in_page import SignInPage
from main.pageobjects.header_bar_component import HeaderBarComponent
from main.pageobjects.settings_page import SettingsPage
from main.pageobjects.new_article_page import NewArticlePage
from main.pageobjects.notifications_component import NotificicationsComponent
from main.pageobjects.home_page import HomePage
from test.session.session_singleton import SessionSingleton

def before_all(context):
    context.session = SessionSingleton.getSession()
    context.driver = context.session.getDriver()
    context.signUpPage = SignUpPage(context.driver)
    context.signInPage = SignInPage(context.driver)
    context.homePage = HomePage(context.driver)
    context.newArticlePage = NewArticlePage(context.driver)
    context.headerBarComponent = HeaderBarComponent(context.driver)
    context.notificicationsComponent = NotificicationsComponent(context.driver)
    context.settingsPage = SettingsPage(context.driver)

def after_all(context):
    context.session.stopDriver()