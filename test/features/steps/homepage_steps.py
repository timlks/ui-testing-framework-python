from behave import *
from test.session.shared_data_keys import USERNAME, TITLE

@when('selects "{feed}" feed')
def selects_feed(context, feed):
    context.homePage.selectFeed(feed)

@then('the article is displayed')
def the_user_is_signed_in(context):
    context.session.waitForAngular()
    assert context.homePage.containsArticle(context.session.sharedData[TITLE])

@then('the user is signed in')
def the_user_is_signed_in(context):
    context.session.waitForAngular()
    assert context.headerBarComponent.isProfileNameDisplayed()
    assert (context.session.sharedData[USERNAME]==context.headerBarComponent.getProfileName())

