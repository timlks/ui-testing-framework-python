from behave import *
from test.navigation.page_selection import Navigation

@step('the user navigates to the "{page}" page')
def when_the_user_navigates_to_the_page(context, page):
    Navigation.goToPage(context, page)
