from behave import *
from test.session.shared_data_keys import PASSWORD, EMAIL
from test.navigation.page_selection import Navigation
from main.datagenerators.random_values_generator import RandomDataGenerator

@step('the user signs in with valid information')
def given_the_user_signs_in_with_valid_information(context):

    if "login" in context.session.getDriver().current_url or context.headerBarComponent.isSignInDisplayed():
        Navigation.goToPage(context, "signIn")

    context.session.waitForAngular()

    context.signInPage.setEmail(context.session.sharedData[EMAIL])
    context.signInPage.setPassword(context.session.sharedData[PASSWORD])

    context.signInPage.clickSignIn()

    context.session.waitForAngular()

@when('the user enters wrong credentials')
def the_user_enters_wrong_credentials(context):

    context.signInPage.setEmail(RandomDataGenerator.randomEmail())
    context.signInPage.setPassword(RandomDataGenerator.randonInt(8))

    context.signInPage.clickSignIn()

    context.session.waitForAngular()

@then('a notification saying "{notification}" is displayed')
def a_notification_saying_is_displayed(context, notification):
    assert context.notificicationsComponent.areNotificationsDisplayed() is True
    assert context.notificicationsComponent.isNotificationDisplayed(notification) is True



