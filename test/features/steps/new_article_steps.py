from behave import *
from test.session.shared_data_keys import TITLE

@when('the user creates a new article with the following information')
def the_user_creates_a_new_article_with_the_following_information(context):

    article = []

    for row in context.table:
        article.append(row["value"])

    tags = article[3].split(",")

    context.newArticlePage.setTitle(article[0])
    context.newArticlePage.setDescription(article[1])
    context.newArticlePage.setBody(article[2])
    context.newArticlePage.setTags(tags)

    context.session.sharedData.update({TITLE:article[0]})

    context.newArticlePage.clickPublishButton()

    context.session.waitForAngular()

