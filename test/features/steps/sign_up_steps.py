from behave import *
from main.dataobjects.user_profile import UserProfile
from main.datagenerators.random_values_generator import RandomDataGenerator
from test.session.shared_data_keys import USERNAME, PASSWORD, EMAIL
from test.navigation.page_selection import Navigation

@given('a user was successfully created')
def a_user_was_successfully_created(context):

    Navigation.goToPage(context, "signUp")

    userProfile = UserProfile(RandomDataGenerator.randonString(), RandomDataGenerator.randonInt(8), RandomDataGenerator.randomEmail())

    context.session.sharedData.update({USERNAME:userProfile.username})
    context.session.sharedData.update({PASSWORD:userProfile.password})
    context.session.sharedData.update({EMAIL:userProfile.email})

    context.signUpPage.createUser(userProfile)

    context.session.waitForAngular()

    context.headerBarComponent.clickSettings()
    context.settingsPage.clickLogout()

    context.session.waitForAngular()

@when('the user adds valid information')
def the_user_add_valid_information(context):
    userProfile = UserProfile(RandomDataGenerator.randonString(), RandomDataGenerator.randonInt(8), RandomDataGenerator.randomEmail())

    context.session.sharedData.update({USERNAME:userProfile.username})
    context.session.sharedData.update({PASSWORD:userProfile.password})
    context.session.sharedData.update({EMAIL:userProfile.email})

    context.signUpPage.createUser(userProfile)