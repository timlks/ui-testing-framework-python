from main.pageobjects.header_bar_component import HeaderBarComponent
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Navigation:

    @staticmethod
    def goToPage(context, page):
        context.driver.get(context.session.getBaseUrl())
        WebDriverWait(context.driver, 10).until(EC.presence_of_element_located((HeaderBarComponent.locator_dictionary["logo"])))

        if page == 'home':
            context.driver.get(context.session.getBaseUrl())
        elif page == 'signIn':
            context.headerBarComponent.clickSignInButton()
        elif page == 'signUp':
            context.headerBarComponent.clickSignUp()
        elif page == 'newArticle':
            context.session.waitForAngular()
            context.headerBarComponent.clickNewArticle()

        context.session.waitForAngular()