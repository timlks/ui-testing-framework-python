import unittest
from parameterized import parameterized
from main.pageobjects.sign_up_page import SignUpPage
from main.pageobjects.header_bar_component import HeaderBarComponent
from main.pageobjects.new_article_page import NewArticlePage
from main.pageobjects.home_page import HomePage
from test.session.session_singleton import SessionSingleton
from main.dataobjects.user_profile import UserProfile
from main.datagenerators.random_values_generator import RandomDataGenerator
import os
from selenium import webdriver

class TestArticles(unittest.TestCase):

    session = None
    newArticlePage = None
    homePage = None
    driver = None
    headerBarComponent = None

    def setUp(self):
        self.session = SessionSingleton.getSession()

        paths = os.environ['Path'].split(";")
        driverPath = None

        for path in paths:
            if "chromedriver" in path:
                driverPath = path

        self.driver = webdriver.Chrome(driverPath)
        self.newArticlePage = NewArticlePage(self.driver)
        self.headerBarComponent = HeaderBarComponent(self.driver)
        self.homePage = HomePage(self.driver)
        signUpPage = SignUpPage(self.driver)

        self.driver.get(self.session.getBaseUrl())
        self.headerBarComponent.clickSignUp()

        userProfile = UserProfile(RandomDataGenerator.randonString(), RandomDataGenerator.randonInt(8),
                                  RandomDataGenerator.randomEmail())

        signUpPage.createUser(userProfile)

        self.session.waitForAngular()

        self.headerBarComponent.clickNewArticle()

    def tearDown(self):
        self.driver.close()

    @parameterized.expand([
        ["title 1", "description 1", "body 1", ],
        ["title 2", "description 2", "body 2"],
        ["title 3", "description 3", "body 3"],
    ])
    def test_createMultipleArticles(self, title, description, body):
        self.newArticlePage.setTitle(title)
        self.newArticlePage.setDescription(description)
        self.newArticlePage.setBody(body)

        self.newArticlePage.clickPublishButton()

        self.session.waitForAngular()

        self.driver.get(self.session.getBaseUrl())

        self.session.waitForAngular()

        self.homePage.selectFeed("global")

        self.session.waitForAngular()

        assert self.homePage.containsArticle(title)

    if __name__ == '__main__':
        unittest.main()